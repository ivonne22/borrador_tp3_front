import { Component, OnInit } from '@angular/core';
import {Cliente} from "../models/cliente";
import {Producto} from "../models/producto";
import {VentaCabecera, VentaDetalle} from "../models/venta";
import {VentaService} from "../shared/servicios/venta.service";
import {MatDialog} from "@angular/material/dialog";
import {ModalCabeceraComponent} from "../shared/componentes/modal-cabecera/modal-cabecera.component";

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  cabeceras!: VentaCabecera[];

  constructor(
    private servicioCabecera: VentaService,
    public dialog: MatDialog,

  ) {
    this.servicioCabecera.cargar();
  }


  ngOnInit():void {
    this.cabeceras = this.servicioCabecera.getListaCabeceras();
    console.log(this.cabeceras);
  }

}
