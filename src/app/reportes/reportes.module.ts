import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportesRoutingModule } from './reportes-routing.module';
import { ReportesComponent } from './reportes.component';
import {SharedModule} from "../shared/shared.module";
import {AppRoutingModule} from "../app-routing.module";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    ReportesComponent
  ],
    imports: [
        CommonModule,
        ReportesRoutingModule,
        SharedModule,
        MatButtonModule,
    ]
})
export class ReportesModule { }
