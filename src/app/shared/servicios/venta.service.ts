import { Injectable } from '@angular/core';
import {Cliente} from "../../models/cliente";
import {VentaCabecera, VentaDetalle} from "../../models/venta";
import {MensajeErrorExitoService} from "./mensaje-error-exito.service";
import {Producto} from "../../models/producto";

const CLIENTE: Cliente = {
  ruc:"123456-0", nombreApellido: "Ciudadano comun", email: "comunacho@gmail.com"
}

const PRODUCTO: Producto = {
  codigo:5, nombre:"Curita", existencia:50 ,precioVenta:5000
}

const PRODUCT: Producto = {
  codigo:3, nombre:"Jabon", existencia:100 ,precioVenta:4000
}

const detalle: VentaDetalle = {producto: PRODUCTO,totalDetalle: 10000, cantidad: 2}

const DETALLES: VentaDetalle[] = [
  {producto: PRODUCTO,totalDetalle: 10000, cantidad: 2},
  {producto: PRODUCT,totalDetalle: 12000, cantidad: 3},
]

const CABECERAS: VentaCabecera[] = [
  {id: 1, fecha: new Date(), numFactura:"001-001-00001", cliente: CLIENTE,total: 120000, detalles: DETALLES},
  {id: 2, fecha: new Date(), numFactura:"001-001-00123", cliente: CLIENTE,total: 50000, detalles: DETALLES},
  {id: 3, fecha: new Date(), numFactura:"001-001-00265", cliente: CLIENTE,total: 80000, detalles: DETALLES},
  {id: 4, fecha: new Date(), numFactura:"001-001-00054", cliente: CLIENTE,total: 20000, detalles: DETALLES},
]

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  clienteVacio: Cliente = {ruc:"", email:"", nombreApellido:""}
  productoVacio: Producto = {codigo:0, nombre:"", precioVenta:0, existencia:0}
  clienteAnonimo: Cliente={ruc:"444444",nombreApellido:"SIN NOMBRE", email:""}
  detalles!: VentaDetalle[];

  cabecera: VentaCabecera = {id:0,cliente:this.clienteVacio,fecha: new Date(),numFactura:"",total:0,detalles:this.detalles};
  cabeceraVacio: VentaCabecera = {id:0,cliente:this.clienteVacio,fecha: new Date(),numFactura:"",total:0,detalles:DETALLES};
  cabeceras!: VentaCabecera[];
  tamanho!: number;
  contadorCabecera!: number;

  constructor(
    private servicioMensaje: MensajeErrorExitoService) {
    this.cargar();
  }

  cargar(){
    if(localStorage.getItem('cabeceras') === null || localStorage.getItem('cabeceras') == undefined){
      let cabs = [
        {id: 1, fecha: new Date(), numFactura:"001-001-00001", cliente: CLIENTE,total: 120000, detalles: DETALLES},
        {id: 2, fecha: new Date(), numFactura:"001-001-00123", cliente: CLIENTE,total: 50000, detalles: DETALLES},
        {id: 3, fecha: new Date(), numFactura:"001-001-00265", cliente: CLIENTE,total: 80000, detalles: DETALLES},
        {id: 4, fecha: new Date(), numFactura:"001-001-00054", cliente: CLIENTE,total: 20000, detalles: DETALLES},
      ];
      localStorage.setItem('cabeceras',JSON.stringify(cabs));
      this.contadorCabecera = 3;
    }
    return
  }

  setCodigoCabecera(codigo: number){
    this.contadorCabecera = codigo;
  }

  getCodigoCabecera():number{
    return this.contadorCabecera;
  }

  getListaCabeceras(): VentaCabecera[]{
    return JSON.parse(<string>localStorage.getItem('cabeceras'));
  }

  crearCabecera(cabecera: VentaCabecera){
    let cabs = JSON.parse(<string>localStorage.getItem('cabeceras'));
    cabs.push(cabecera);
    localStorage.setItem('cabeceras',JSON.stringify(cabs));
    this.cabecera = this.cabeceraVacio;
  }

  eliminarCabecera(id: number) {
    let cabs = JSON.parse(<string>localStorage.getItem('cabeceras'));

    for(let i = 0; i <cabs.length; i++) {
      if(cabs[i].codigo == id) {
        cabs.splice(i, 1);
      }
    }
    localStorage.setItem('cabeceras', JSON.stringify(cabs));
  }

  modificarCabecera(id: number,cabecera:VentaCabecera) {
    let cabs = JSON.parse(<string>localStorage.getItem('cabeceras'));

    for(let i = 0; i <cabs.length; i++) {
      if(cabs[i].codigo == id) {
        cabs[i] = cabecera;
      }
    }
    localStorage.setItem('cabeceras', JSON.stringify(cabs));
  }


}
