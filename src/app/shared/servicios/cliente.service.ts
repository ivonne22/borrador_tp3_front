import { Injectable } from '@angular/core';
import {Cliente} from "../../models/cliente";
import {MensajeErrorExitoService} from "./mensaje-error-exito.service";


@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  cliente: Cliente = {ruc:"",nombreApellido:"",email:""};
  clienteVacio: Cliente = {ruc:"",nombreApellido:"",email:""};
  clientes!: Cliente[];
  tamanho!: number;

  constructor(
    private servicioMensaje: MensajeErrorExitoService) {
    this.cargar();
  }

  cargar(){
    if(localStorage.getItem('clientes') === null || localStorage.getItem('clientes') == undefined){
      let cliens = [
        {ruc: "1234567-8", nombreApellido: "Natalia Cardozo", email: "natcar@gmail.com"},
        {ruc: "564213-4", nombreApellido: "Fernando Caballero", email: "fercab@gmail.com"},
        {ruc: "4652348-1", nombreApellido: "Benjamin Racchi", email: "benrac@gmail.com"},
        {ruc: "6598563-4", nombreApellido: "Ernesto Moran", email: "ernmor@gmail.com"},
        {ruc: "9568452-7", nombreApellido: "Ivonne Rolon", email: "ivorol@gmail.com"},
      ];
      localStorage.setItem('clientes',JSON.stringify(cliens));
    }
    return
  }

  getListaClientes(): Cliente[]{
    return JSON.parse(<string>localStorage.getItem('clientes'));
  }

  crearCliente(ruc: string, nombre:string, email: string){
    this.cliente.ruc = ruc;
    this.cliente.nombreApellido = nombre;
    this.cliente.email = email;
    let cliens = JSON.parse(<string>localStorage.getItem('clientes'));
    cliens.push(this.cliente);
    localStorage.setItem('clientes',JSON.stringify(cliens));
    this.cliente = this.clienteVacio;
  }

  eliminarCliente(id: string) {
    let cliens = JSON.parse(<string>localStorage.getItem('clientes'));

    for(let i = 0; i <cliens.length; i++) {
      if(cliens[i].ruc == id) {
        cliens.splice(i, 1);
      }
    }
    localStorage.setItem('clientes', JSON.stringify(cliens));
  }

  modificarCliente(id: string,cliente:Cliente) {
    console.log(cliente);
    let cliens = JSON.parse(<string>localStorage.getItem('clientes'));

    for(let i = 0; i <cliens.length; i++) {
      if(cliens[i].ruc == id) {
        cliens[i] = cliente;
      }
    }
    localStorage.setItem('clientes', JSON.stringify(cliens));
  }



}
