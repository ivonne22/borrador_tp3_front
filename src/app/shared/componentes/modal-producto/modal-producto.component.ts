import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

interface Resultado{
  nombre: string,
  precioVenta: number,
  existencia: number,
  modificar: boolean
}


@Component({
  selector: 'app-modal-producto',
  templateUrl: './modal-producto.component.html',
  styleUrls: ['./modal-producto.component.css']
})
export class ModalProductoComponent implements OnInit {


  constructor(
    public dialogRef: MatDialogRef<ModalProductoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Resultado,
) { }


  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
