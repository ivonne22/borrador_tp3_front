import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {VentaCabecera, VentaDetalle} from "../../../models/venta";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {Cliente} from "../../../models/cliente";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {VentaService} from "../../servicios/venta.service";
import {MatDialog} from "@angular/material/dialog";
import {ModalCabeceraComponent} from "../modal-cabecera/modal-cabecera.component";

@Component({
  selector: 'app-reporte-ventas-resumido',
  templateUrl: './reporte-ventas-resumido.component.html',
  styleUrls: ['./reporte-ventas-resumido.component.css']
})
export class ReporteVentasResumidoComponent implements OnInit {
  @Input() cabeceras !: VentaCabecera[];

  displayedColumns: string[] = ['numFactura', 'fecha','cliente','total'];
  dataSource!: MatTableDataSource<VentaCabecera>;


  detalles!: VentaDetalle[];


  @ViewChild(MatTable) table!: MatTable<VentaCabecera>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    public dialog: MatDialog,
    private servicioCabecera: VentaService,
  ) {

  }

  ngOnInit(){
    this.dataSource = new MatTableDataSource(this.cabeceras);
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
