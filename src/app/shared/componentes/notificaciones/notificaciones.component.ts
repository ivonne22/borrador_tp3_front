import { Component, OnInit } from '@angular/core';
import {MensajeErrorExitoService} from "../../servicios/mensaje-error-exito.service";

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.component.html',
  styleUrls: ['./notificaciones.component.css']
})
export class NotificacionesComponent implements OnInit {

    mensajeError!:string;
    mensajeExito!: string;

  constructor(private servicioNotificacion: MensajeErrorExitoService) { }

  ngOnInit(): void {
    this.mensajeError = this.servicioNotificacion.mensajeError;
    this.mensajeExito = this.servicioNotificacion.mensajeExito;
  }

}
