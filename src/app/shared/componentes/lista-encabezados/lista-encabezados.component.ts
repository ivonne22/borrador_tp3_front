import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {VentaCabecera, VentaDetalle} from "../../../models/venta";
import {VentaService} from "../../servicios/venta.service";
import {Cliente} from "../../../models/cliente"
import {Producto} from "../../../models/producto";
import {ModalCabeceraComponent} from "../modal-cabecera/modal-cabecera.component";
import {animate, state, style, transition, trigger} from "@angular/animations";
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-lista-encabezados',
  templateUrl: './lista-encabezados.component.html',
  styleUrls: ['./lista-encabezados.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],

})
export class ListaEncabezadosComponent implements OnInit {

  @Input() cabeceras !: VentaCabecera[];

  displayedColumns: string[] = ['id', 'numFactura', 'fecha','cliente','total', 'editar', 'eliminar'];
  dataSource!: MatTableDataSource<VentaCabecera>;
  expandedElement!: VentaCabecera | null;


  clienteVacio: Cliente = {ruc:"", nombreApellido:"", email:""}
  detalles!: VentaDetalle[];
  cabeceraNueva!: VentaCabecera;
  clienteAnonimo: Cliente={ruc:"444444",nombreApellido:"SIN NOMBRE", email:""}


  @ViewChild(MatTable) table!: MatTable<VentaCabecera>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    private servicioCabecera: VentaService,
    public dialog: MatDialog,
  ) {

  }

  ngOnInit(){
    this.dataSource = new MatTableDataSource(this.cabeceras);
    console.log(this.dataSource);
  }

  ngOnChanges(){
    this.dataSource = new MatTableDataSource(this.cabeceras);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  eliminarCabecera(id: number){
    this.servicioCabecera.eliminarCabecera(id);
    this.dataSource = new MatTableDataSource(this.servicioCabecera.getListaCabeceras());
  }

  modificarCabecera(cabecera: VentaCabecera){
    this.cabeceraNueva.id = cabecera.id;
    this.openDialog(cabecera.fecha,cabecera.cliente,cabecera.total,cabecera.numFactura,cabecera.detalles);
  }

  openDialog(fecha:Date, cliente: Cliente, total:number, factura:string, detalles:VentaDetalle[]): void {
    const dialogRef = this.dialog.open(ModalCabeceraComponent, {
      width: '500px',
      data: {fecha: fecha, cliente: cliente, total: total, factura:factura, detalles:detalles, modificar:true},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.numFact == undefined) {
        //no se agrego, se debe agregar un numero de factura si o si
      } else {
        this.cabeceraNueva.numFactura = result.numFactura;
        if(result.fecha == undefined){
          this.cabeceraNueva.fecha = new Date();
        } else {
          this.cabeceraNueva.fecha = result.fecha;
        }
        if (result.cliente != this.clienteVacio) {
          this.cabeceraNueva.cliente = result.cliente;
        } else {
          this.cabeceraNueva.cliente = this.clienteAnonimo;
        }
        if (result.total != 0) {
          this.cabeceraNueva.total = result.total;
        }
      }
      this.servicioCabecera.modificarCabecera(this.cabeceraNueva.id, this.cabeceraNueva);
      this.dataSource = new MatTableDataSource(this.servicioCabecera.getListaCabeceras());
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
