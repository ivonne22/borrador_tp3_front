import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { ListaProductosComponent } from './componentes/lista-productos/lista-productos.component';
import {MatTableModule} from "@angular/material/table";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatPaginatorModule} from "@angular/material/paginator";
import { ListaClientesComponent } from './componentes/lista-clientes/lista-clientes.component';
import { ListaDetallesComponent } from './componentes/lista-detalles/lista-detalles.component';
import { ListaEncabezadosComponent } from './componentes/lista-encabezados/lista-encabezados.component';
import { ModalProductoComponent } from './componentes/modal-producto/modal-producto.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import { NotificacionesComponent } from './componentes/notificaciones/notificaciones.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import { ModalClienteComponent } from './componentes/modal-cliente/modal-cliente.component';
import {ModalCabeceraComponent} from "./componentes/modal-cabecera/modal-cabecera.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatDatepickerModule} from "@angular/material/datepicker";
import { ReporteVentasResumidoComponent } from './componentes/reporte-ventas-resumido/reporte-ventas-resumido.component';
import { ReporteVentasDetalladoComponent } from './componentes/reporte-ventas-detallado/reporte-ventas-detallado.component';



@NgModule({
  declarations: [
    ListaProductosComponent,
    ListaClientesComponent,
    ListaDetallesComponent,
    ListaEncabezadosComponent,
    ModalProductoComponent,
    NotificacionesComponent,
    ModalClienteComponent,
    ModalCabeceraComponent,
    ReporteVentasResumidoComponent,
    ReporteVentasDetalladoComponent
  ],
    imports: [
        CommonModule,
        MatTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatPaginatorModule,
        MatDialogModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        MatToolbarModule,
        MatIconModule,
        MatDatepickerModule,

    ],
  exports: [ListaProductosComponent, ListaClientesComponent, ListaEncabezadosComponent, ReporteVentasResumidoComponent, ReporteVentasDetalladoComponent],
  providers: [DatePipe]
})
export class SharedModule { }
