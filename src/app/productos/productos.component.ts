import { Component, OnInit } from '@angular/core';
import {ProductoService} from "../shared/servicios/producto.service";
import {Producto} from "../models/producto";
import {MatDialog} from "@angular/material/dialog";
import {ModalProductoComponent} from "../shared/componentes/modal-producto/modal-producto.component";

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  productos!: Producto[];
  nombre!: string;
  precioVenta!: number;
  existencia!: number;


  constructor(
    private servicioProducto: ProductoService,
    public dialog: MatDialog,

  ) {
    this.servicioProducto.cargar();
    if(this.servicioProducto.contadorProducto === null || this.servicioProducto.contadorProducto == undefined){
      this.servicioProducto.contadorProducto = 1;
    } else {
      ++this.servicioProducto.contadorProducto;
    }
  }


    ngOnInit():void {

      this.productos = this.servicioProducto.getListaProductos();
    }

    recargarLista(){
      this.productos = this.servicioProducto.getListaProductos();
    }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalProductoComponent, {
      width: '250px',
      data: {nombre: "", precioVenta: 0, existencia: 0, modificar:false},
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.nombre == undefined) {
        this.nombre = "";
      } else {
        this.nombre = result.nombre;
        if(result.precioVenta == undefined) {
          this.precioVenta = 0;
        } else {
          this.precioVenta = result.precioVenta;
        }
        if(result.existencia == undefined) {
          this.existencia = 0;
        } else {
          this.existencia = result.existencia;
        }
        ++this.servicioProducto.contadorProducto;
        this.servicioProducto.crearProducto(this.servicioProducto.contadorProducto,this.nombre,this.precioVenta,this.existencia);
      }
      this.recargarLista();
    });
  }


}
