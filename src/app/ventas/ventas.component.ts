import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {VentaCabecera, VentaDetalle} from "../models/venta";
import {Cliente} from "../models/cliente";
import {Producto} from "../models/producto";
import {VentaService} from "../shared/servicios/venta.service";
import {ModalCabeceraComponent} from "../shared/componentes/modal-cabecera/modal-cabecera.component";

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

  clienteVacio: Cliente = {ruc:"", email:"", nombreApellido:""}
  productoVacio: Producto = {codigo:0, nombre:"", precioVenta:0, existencia:0}
  fechaHoy = new Date();
  cabeceras!: VentaCabecera[];
  ruc!: string;
  nombreApellido!: string;
  email!: string;
  cabeceraNueva!: VentaCabecera;
  clienteAnonimo: Cliente={ruc:"444444",nombreApellido:"SIN NOMBRE", email:""}
  detalles!: VentaDetalle[];


  constructor(
    private servicioCabecera: VentaService,
    public dialog: MatDialog,

  ) {
    this.servicioCabecera.cargar();
  }


  ngOnInit():void {

    this.cabeceras = this.servicioCabecera.getListaCabeceras();
  }

  recargarLista(){
    this.cabeceras = this.servicioCabecera.getListaCabeceras();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalCabeceraComponent, {
      width: '800px',
      height: '700px',
      data: {fecha: this.fechaHoy, numFact: "", cliente: this.clienteVacio, total:0, detalles:this.detalles, modificar:false},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.numFact == undefined) {
        //no se agrego, se debe agregar un numero de factura si o si
      } else {
        this.cabeceraNueva.numFactura = result.numFactura;
        if(result.fecha == undefined){
          this.cabeceraNueva.fecha = this.fechaHoy;
        } else {
          this.cabeceraNueva.fecha = result.fecha;
        }
        if (result.cliente != this.clienteVacio) {
          this.cabeceraNueva.cliente = result.cliente;
        } else {
          this.cabeceraNueva.cliente = this.clienteAnonimo;
        }
        if (result.total != 0) {
          this.cabeceraNueva.total = result.total;
        }
        this.cabeceraNueva.detalles = result.detalles;
        this.servicioCabecera.crearCabecera(this.cabeceraNueva);
      }
      this.recargarLista();
    });
  }

}
